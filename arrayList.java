import java.util.Arrays;


public class arrayList<T>{
	private int size_arr = 16;
	private int cut_arr = 4;
	private Object[] array = new Object[size_arr];
	private int counter = 0;

	
	public void add(T item){
		if(counter == array.length - 1) {
			new_size(array.length * 2);
		}
		array[counter] = item;
		counter++;
	}

	
	public T get(int index){
		return (T) array[index];
	}

	
	public void remove(int index){
		for(int i = index; i < counter; i++)
			array[i] = array[i + 1];
		array[counter] = null;
		counter--;
		if(array.length > size_arr && counter < array.length / cut_arr) {
			new_size(array.length / 2);
		}
	}

	
	public void remove(Object ob){
		int index = Arrays.asList(array).indexOf(ob);
		for (int i = index; i < counter; i++) {
			array[i] = array[i + 1];
		}
		array[counter] = null;
		counter--;
		if (array.length > size_arr && counter < array.length / cut_arr) {
			new_size(array.length / 2);
		}
	}

	
	public void clear(){
		array = new Object[size_arr];
		counter = 0;
	}
	
	
	public int size(){
        return counter;
    }
	
	
	public boolean contains(Object ob){
		boolean flag = false;
		for(int i = 0; i < counter; i++){
			if (array[i] == ob) {
				flag = true;
				break;
			}
		}
		return flag;
	}
	
	
	public boolean isEmpty(){
		return counter <= 0;
	}
	
	
	private void new_size(int new_length){
		Object[] new_array = new Object[new_length];
		System.arraycopy(array, 0, new_array, 0, counter);
		array = new_array;
	}
}